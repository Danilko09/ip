rem web-server
cd ..
docker-compose -f docker-compose.yml -f docker-compose.tests.yml up -d

rem local chromedriver
cd tests
java -Dwebdriver.chrome.driver=selenium-standalone/chromedriver.exe -jar selenium-standalone/selenium-server-standalone-3.141.59.jar -role webdriver -hub http://localhost:4444/grid/register -port 5558 -browser browserName=chrome
