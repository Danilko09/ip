<?php 

foreach(scandir(__DIR__) as $entry){
	if(isTest($entry))
		echo '<a href="'.$entry.'">'.$entry.'</a><br/>';
}

function isTest($file){
	$suffix = 'Test.php';
	return strpos($file, $suffix) == (strlen($file) - strlen($suffix));
}
