<?php

namespace pageobjects;

abstract class BasePageObject {

	protected $element;
	private $elements = [];

	protected function __construct($element){
		if(is_null($element)) throw new \Exception("Element not found");
		$this->element = $element;
	}

	protected function setElement($name, $elem){
		if(is_null($elem)) throw new \Exception("Element cannot be NULL");
		$this->elements[$name] = $elem;
	}

	protected function setElements($elems){
		foreach($elems as $name => $elem)
			$this->setElement($name, $elem);
	}

	function __get($name){
		if(!isset($this->elements[$name])) throw new \Exception("Field $name not found");
		return $this->elements[$name];	
	}

	function __call($method, $args){
		return call_user_func_array([$this->element, $method], $args);
	}

	function setField($field, $value){
		if(!isset($this->elements[$field.'Field'])) 
			throw new \Exception('Unknown field '.$field);
		testLog('setting field "'.$field.'" to "'.$value.'" in '.get_class($this));
		$this->elements[$field.'Field']->clear()->sendKeys($value);
	}

	function click($element){
		testLog('clicking '.$element.' on '.get_class($this));
		$this->$element->click();
	}

}
