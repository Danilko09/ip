<?php

namespace pageobjects;

use Facebook\WebDriver\WebDriverBy as By;
use utils\LazyObject;

class HeadMenu extends SingletonBasePageObject {

	function __construct(){
		global $driver;
		parent::__construct($driver->findElement(
			By::cssSelector('.navbar.sticky-top')
		));

		$this->setElements([
			'loginButton' => 
				$this->findElement(By::id('loginFormDropdown')),

			'loginForm' => new LazyObject('\pageobjects\LoginForm'),
			'logoutButton' => 
				$this->findElement(By::id('logoutLink')),
		]);
	}

}
