<?php

namespace pageobjects;

use \Facebook\WebDriver\WebDriverBy as By;

class LoginForm extends BasePageObject {

	function __construct(){
		parent::__construct(HeadMenu::get()->findElement(
			By::cssSelector('[aria-labelledby="loginFormDropdown"]')
		));

		$this->setElements([
			'loginField' => $this->findElement(By::id('login')),	
			'passwordField' => $this->findElement(By::id('password')),	
			'loginButton' => $this->findElement(By::id('loginButton')),
			'badLoginAlert' => $this->findElement(By::id('badLoginAlert')),
		]);
	}

	function submit(){
		$this->click('loginButton');
	}

}
