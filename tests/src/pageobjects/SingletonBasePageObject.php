<?php

namespace pageobjects;

abstract class SingletonBasePageObject extends BasePageObject {


	private static $instance = null;
	static function get(){
		if(!self::$instance) self::$instance = new static();
		return self::$instance;
	}

}
