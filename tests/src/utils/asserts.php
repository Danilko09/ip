<?php

	ini_set('zend.assertions', 1);
	ini_set('assert.exception', 1);

	function assertFalse($actual, $message){
		assert($actual === false, $message);
	}

	function assertTrue($actual, $message){
		assert($actual === true, $message);
	}


