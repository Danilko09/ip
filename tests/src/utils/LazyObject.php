<?php

namespace utils;

class LazyObject {

	private $class;

	function __construct($class){
		$this->class = $class;
	}

	private $instance = null;

	private function instantinate(){
		if(!$this->instance){
			$className = $this->class;
			$this->instance = new $className;
		}
	}

	function __call($method, $args){
		$this->instantinate();	
		return call_user_func_array([$this->instance, $method], $args);
	}

	function __get($field){
		return $this->instance->$field;
	}

}
