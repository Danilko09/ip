<?php

namespace utils;

class DBHelpers {

	
	private static $con = null;

	public static function getConnection(){
		if(is_null(self::$con)){
			define('marker', 1);
			self::$con = new \PDO(
				'mysql:dbname='.$_ENV['DB_NAME'].
				';host='.$_ENV['DB_HOST'].
				';port='.$_ENV['DB_PORT'], 
				$_ENV['DB_USER'], 
				$_ENV['DB_PASS']
			);
		}
		return self::$con;
	}

	public static function createUser($login, $pass, $role){
		testLog("Creating user (login: $login,pass: $pass,role: $role)");
		$stmt = self::getConnection()->prepare('
			INSERT IGNORE INTO users 
			(login, password, role)
			VALUES (:login, :pass, :role)
		');		
		$stmt->bindValue(':login', $login);
		$stmt->bindValue(':pass', password_hash($pass, PASSWORD_DEFAULT));
		$stmt->bindValue(':role', $role);
		$stmt->execute();
	}

}
