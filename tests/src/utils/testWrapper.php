<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

include_once __DIR__.DIRECTORY_SEPARATOR.'autoload.php';
include_once __DIR__.DIRECTORY_SEPARATOR.'asserts.php';

$driver = null;

function testWrapper($test){
	global $driver;
	testLog('connecting to selenium hub');
	$wd_url = 'http://'.$_ENV['HUB_HOST'].':'.$_ENV['HUB_PORT'].'/wd/hub';
	$driver = RemoteWebDriver::create($wd_url, 
			DesiredCapabilities::chrome(), 2000);
	register_shutdown_function(function() use ($driver){
		$driver->quit();
	});
	$driver->manage()->timeouts()->implicitlyWait(5);
	testLog('opening application endpoint ('.$_ENV['ENDPOINT'].')');
	$driver->get($_ENV['ENDPOINT']);	
	testLog('starting test');
	try {
		$test($driver);
		testLog('Test finished successfully', 'RESULT');
	}catch(\Exception $e){
		testLog("Caught exception: ".$e->getMessage()."\n".
		$e->getTraceAsString());
		testLog('Test FAILED', 'RESULT');
	}
}

function testLog($message, $level = 'INFO'){
	if(in_array($level, ['STEP', 'RESULT'])) echo '<br/>';
	echo '['.$level.'] '.str_replace("\n", '<br/>', $message).'<br/>';
}
