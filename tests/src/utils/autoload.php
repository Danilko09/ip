<?php
spl_autoload_register(function ($class_name) {
	foreach([
		'Facebook\WebDriver' 	=> dirname(__DIR__).DIRECTORY_SEPARATOR.'wd',
		'pageobjects'		=> dirname(__DIR__).DIRECTORY_SEPARATOR.'pageobjects',
		'utils'			=> dirname(__DIR__).DIRECTORY_SEPARATOR.'utils',
	] as $prefix => $dir){
	if(strpos($class_name, $prefix) === 0)
		include($dir.str_replace('\\', DIRECTORY_SEPARATOR, 
				substr($class_name, strlen($prefix))
			).'.php');
	}
});
