<?php

use pageobjects\HeadMenu;
use utils\DBHelpers as DBH;
use Facebook\WebDriver\WebDriverExpectedCondition as Condition;

include 'utils/testWrapper.php';

testWrapper(function($driver){
	assertFalse(HeadMenu::get()->loginForm->isDisplayed(), 
			"Login form should be hidden by default");

	testLog('Submit form with empty fields', 'STEP');

	HeadMenu::get()->click('loginButton');
	assertTrue(HeadMenu::get()->loginForm->isDisplayed(),
			"Login form should be displayed after menu click");
	assertFalse(HeadMenu::get()->loginForm->badLoginAlert->isDisplayed(),
			"Bad login alert should be hidden by default");

	HeadMenu::get()->loginForm->submit();
	$driver->wait(10, 100)->until(
		Condition::visibilityOf(HeadMenu::get()->loginForm->badLoginAlert)
	);
	assertTrue(HeadMenu::get()->loginForm->badLoginAlert->isDisplayed(),
			"Bad login alert should be visible if empty form submitted");

	testLog('Log in as real user with correct password', 'STEP');

	DBH::createUser('login', 'password', 'admin');
	HeadMenu::get()->loginForm->setField('login', 'login');
	HeadMenu::get()->loginForm->setField('password', 'password');
	HeadMenu::get()->loginForm->submit();

	$driver->wait(10, 100)->until(function(){
		return !HeadMenu::get()->loginForm->isDisplayed();
	}, "Waiting disappear of login form");
	assertTrue(HeadMenu::get()->logoutButton->isDisplayed(),
			"Logout button should be visible after logging in");
	assertFalse(HeadMenu::get()->loginButton->isDisplayed(),
			"Login button should be invisible after logging in");

	HeadMenu::get()->click('logoutButton');
	assertFalse(HeadMenu::get()->logoutButton->isDisplayed(),
			"Logout button should be invisible after logging out");
	assertTrue(HeadMenu::get()->loginButton->isDisplayed(),
			"Login button should be visible after logging out");
	
	testLog('Log in as real user with incorrect password', 'STEP');
	HeadMenu::get()->click('loginButton');
	DBH::createUser('login', 'password', 'admin');
	HeadMenu::get()->loginForm->setField('login', 'login');
	HeadMenu::get()->loginForm->setField('password', 'bad password');
	HeadMenu::get()->loginForm->submit();
	$driver->wait(10, 100)->until(
		Condition::visibilityOf(HeadMenu::get()->loginForm->badLoginAlert)
	);
	sleep(2);
	assertFalse(HeadMenu::get()->logoutButton->isDisplayed(),
			"Logout button should be invisible");
	assertTrue(HeadMenu::get()->loginForm->badLoginAlert->isDisplayed(),
			"Bad login alert should be visible if bad pass submitted");
});
