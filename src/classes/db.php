<?php

	class db {
		
		private static $con = null;

		public static function getConnection(){
			if(is_null(self::$con)){
				define('marker', 1);
				$config = include dirname(__DIR__).DIRECTORY_SEPARATOR.'dbconfig.php';
				self::$con = new PDO('mysql:dbname='.$config['base'].';host='.$config['host'].';port='.$config['port'], $config['user'], $config['pass']);
			}
			return self::$con;
		}

	}
