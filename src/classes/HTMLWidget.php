
abstract class HTMLWidget {

	/**
	 *  Производит генерацию html-кода виджета для последующей отправки в браузер 
	 */
	abstract public function render();
}
