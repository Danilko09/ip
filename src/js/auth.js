$("#logoutLink").on('click', function(){
	api.logout(function(){
		api.isLogged(onAuthUpdate);
	});
	return false;
});


$("#loginSpinner").hide();
$("#loginForm").on('submit', function(){
	$("#loginButton").slideUp();
	$("#loginSpinner").slideDown();
	api.login($("#login").val(), $("#password").val(), function(){
		api.isLogged(function(isLogged){
			$("#loginSpinner").slideUp();
			if(isLogged){
				$("#badLoginAlert").slideUp();
				var dropdown = $("#loginForm").parent();
				dropdown.fadeOut(function(){
					dropdown.removeClass("show");
					dropdown.css("display", "");
					$("#loginButton").show();
					onAuthUpdate(isLogged);
				});
			}else{
				$("#loginButton").slideDown(function(){
					$("#badLoginAlert").slideDown();
				});
			}
		});
	});
	return false;
});

var subscribers = [];
function subscribeOnAuthUpdate(callback){
	subscribers.push(callback);
}

function onAuthUpdate(isLogged){
	subscribers.forEach(function(callback){
		callback(isLogged);
	});
}

subscribeOnAuthUpdate(function(isLogged){
	$("#loginLabel").fadeOut();
	$("#logoutLink").hide();
	$("#loginFormDropdown").hide();
	if(isLogged){
		$("#logoutLink").show();
		api.getLogin(function(login){
			$("#loginLabel").text(login).fadeIn();
		});
	}else
		$("#loginFormDropdown").show();
});

subscribeOnAuthUpdate(function(isLogged){
	if(isLogged){
		api.isAdmin(function(isAdmin){
			if(isAdmin){
				$("#adminLink").show();
			}else{
				$("#adminLink").hide();
			}
		});
	}else{
		$("#adminLink").hide();
	}
});

$(document).ready(function(){
	api.isLogged(onAuthUpdate);
});
