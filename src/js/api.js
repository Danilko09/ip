	var api = {
		endpoint: "/api",

	//auth
		login: function(login, passw, callback, failCallback = api.error){
			$.post(this.endpoint+'/login.php', {'login': login, 'password': passw}).done(callback).fail(failCallback);
		},

		logout: function(callback, failCallback = api.error){
			$.get(this.endpoint+'/logout.php').done(callback).fail(failCallback);
		},

		isLogged: function(callback, failCallback = api.error){
			$.getJSON(this.endpoint+'/isLogged.php', {},callback).fail(failCallback);
		},

		isAdmin: function(callback, failCallback = api.error){
			$.getJSON(this.endpoint+'/isAdmin.php', {}, callback).fail(failCallback);
		},

		getLogin: function(callback, failCallback = api.error){
			$.get(this.endpoint+'/getLogin.php').done(callback).fail(failCallback);
		},

	//holidays
		getHolidays: function(from, to, callback, failCallback = api.error){
			$.getJSON(
				this.endpoint+'/getHolidays.php'+
				'?from='+encodeURIComponent(from.getTime() / 1000)+
				'&to='+encodeURIComponent(to.getTime() / 1000)
			).done(callback).fail(failCallback);
		},

	//vacations
		getVacations: function(from, to, callback, userid = null, failCallback = api.error){
			$.getJSON(
				this.endpoint+'/getVacations.php'+
				'?from='+encodeURIComponent(from.getTime() / 1000)+
				'&to='+encodeURIComponent(to.getTime() / 1000)+
				(userid != null ? '&userid='+userid : '')
			).done(callback).fail(failCallback);
		},

		addVacation: function(date, callback, userid = null, failCallback = api.error){
			$.post(this.endpoint+'/addVacation.php', {
				'date': date,
				'userid': userid
			}).done(callback).fail(failCallback);
		},

		delVacation: function(date, callback, userid = null, failCallback = api.error){
			$.post(this.endpoint+'/delVacation.php', {
				'date': date,
				'userid': userid
			}).done(callback).fail(failCallback);
		},
		
	//sick
		getSick: function(from, to, callback, userid = null, failCallback = api.error){
			$.getJSON(
				this.endpoint+'/getSick.php'+
				'?from='+encodeURIComponent(from.getTime() / 1000)+
				'&to='+encodeURIComponent(to.getTime() / 1000)+
				(userid != null ? '&userid='+userid : '')
			).done(callback).fail(failCallback);
		},

		addSick: function(date, callback, userid = null, failCallback = api.error){
			$.post(this.endpoint+'/addSick.php', {
				'date': date,
				'userid': userid
			}).done(callback).fail(failCallback);
		},

		delSick: function(date, callback, userid = null, failCallback = api.error){
			$.post(this.endpoint+'/delSick.php', {
				'date': date,
				'userid': userid
			}).done(callback).fail(failCallback);
		},

	//other stuff
		error: function(){
			$("#apiErrorToast").toast('show');	
		}

	};
