var adminApi = {
	endpoint: "/api/admin",
	
	getUsers: function(callback, failCallback = api.error){
		$.getJSON(this.endpoint+"/getUsers.php", {}, callback).fail(failCallback);
	},
	
	addHoliday: function(date, callback, failCallback = api.error){
		$.post(this.endpoint+'/addHoliday.php', {
			'date': date,
		}).done(callback).fail(failCallback);
	},

	delHoliday: function(date, callback, failCallback = api.error){
		$.post(this.endpoint+'/delHoliday.php', {
			'date': date,
		}).done(callback).fail(failCallback);
	},

};
