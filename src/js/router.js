var router = {

	routes: {
		'/': function(){router.open("/index.html");},
		'/index.html': function(){$(".container").load("/index.tmpl.html");},
		'/vacations.html': function(){$(".container").load("/vacations.tmpl.html");},
		'/sick.html': function(){$(".container").load("/sick.tmpl.html");},
		'/holidays.html': function(){$(".container").load("/holidays.tmpl.html");},
		'/admin.html': function(){$(".container").load("/admin.tmpl.html");},

		'/runs.html': function(){$(".container").load("/runs.tmpl.html");},
		'/run.html': function(){$(".container").load("/run.tmpl.html");},
		'/fail.html': function(){$(".container").load("/fail.tmpl.html");},
	},

	set: function(uri, callback){
		this.routes[uri] = callback;
	},

	open: function(query){
		this.loadPage(query);
		history.pushState([], document.title, query);
	},

	loadPage: function(query){
		var uri = query.split("?")[0];
		if(this.routes[uri] == undefined) return;

		$("#main-container").html('\
		<div class="d-flex justify-content-center align-items-center" style="height: 80vh;">\
		  <div class="spinner-border" role="status">\
		    <span class="sr-only">Загрузка...</span>\
		  </div>\
		</div>');

		this.routes[uri]();
	},

	getQueryParameters: function(){
		var params = [];
		document.location.search.substring(1).split("&").forEach(function(el){
			var name = el.split("=")[0];
			params[name] = el.substring(name.length+1);
		});

		return params;
	}

}

$(document).ready(function(){
	router.open(document.location.pathname+document.location.search);
	$('a').on('click', function(e){
		var a_href = e.target.getAttribute('href');
		if(router.routes[a_href] != undefined){
			$('a.active').removeClass('active');
			router.open(a_href);
			e.preventDefault();
			$(e.target).addClass('active');
		}
	});
	$(window).on('popstate', function(){
		router.loadPage(this.document.location.pathname+this.document.location.search);
	});
});
