var calendarUtils = {
	
	generateMonthTable: function(year, month, formatter){
		var dt = new Date(year, month);
		var tbody = $('<tbody>');

		var days = $('<tr>');
		if(dt.getDay() > 1){
			$('<td>',{colspan: dt.getDay()-1}).appendTo(days);
		}else if(dt.getDay() == 0){
			$('<td>', {colspan: 6}).appendTo(days);
		}

		var monthn = dt.getMonth();
		while(monthn == dt.getMonth()){
			for(var i = dt.getDay(); i <= 7; i++){
				formatter($('<td>').appendTo(days), new Date(dt.getTime()));
				dt.setDate(dt.getDate()+1);
				if(monthn != dt.getMonth() || i == 0) break;
			}
			if(monthn == dt.getMonth()){
				days.appendTo(tbody);
				days = $('<tr>');
			}
		}

		if(dt.getDay() < 7 && dt.getDay() != 1){
			$('<td>', {colspan: 8-dt.getDay()}).appendTo(days);
		}
		days.appendTo(tbody);	

		return $('<table>', {"class":"table table-bordered",
			html: [
				$('<thead>',{ html:[
					$('<th>').text('пн'),
					$('<th>').text('вт'),
					$('<th>').text('ср'),
					$('<th>').text('чт'),
					$('<th>').text('пт'),
					$('<th>').text('сб'),
					$('<th>').text('вс'),
				]}),
				tbody
			]
		});
	}

}
