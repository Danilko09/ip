<?php
	$text = mb_strtolower(file_get_contents("zi.txt"));
	$length  = strlen($text);

	$counts = [];

	for($i = 0; $i < $length; $i++){
		$char = mb_substr($text, $i, 1);
		if(!isset($counts[$char]))
			$counts[$char] = 1;
		else
			$counts[$char]++;
	}

	arsort($counts);

	echo '<style>table {
		float: left; 
		border: solid black 1px; 
		margin-right: 10px;}</style>';

	for($i = 0; $i < count($counts); $i += 25){
		echo '<table>
		<tr>
			<th>Символ</th>
			<th>Встречен в тексте раз</th>
			<th>Встречен в тексте %</th>
		</tr>';
		foreach(array_slice($counts, $i, 25) as $char => $count){
			echo '<tr>
				<td>'.$char.'</td>
				<td>'.$count.'</td>
				<td>'.round($count/$length*100, 2).'</td>
			</tr>';
		}
		echo '</table>';
	}
