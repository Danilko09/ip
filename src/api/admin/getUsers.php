<?php
	include '../wrapper.php';
	apiWrapper(function(){
		if(!isAdmin()) forbidden();
		
		$stmt = db::getConnection()->prepare("SELECT id, username, login FROM users");
		$stmt->execute();
		echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
	});
