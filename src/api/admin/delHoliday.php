<?php
	include '../wrapper.php';
	apiWrapper(function(){
		$date = $_POST['date'];
		if(isAdmin()){
			$stmt = db::getConnection()->prepare("DELETE FROM holidays WHERE date = STR_TO_DATE(:date, '%e.%c.%Y')");
			$stmt->bindValue(':date', $date);
			$stmt->execute();
		}else
			forbidden();
	});
