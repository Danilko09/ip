<?php
	include '../wrapper.php';
	apiWrapper(function(){
		$date = $_POST['date'];
		if(isAdmin()){
			$stmt = db::getConnection()->prepare("INSERT IGNORE INTO holidays VALUES (STR_TO_DATE(:date, '%e.%c.%Y'))");
			$stmt->bindValue(':date', $date);
			$stmt->execute();
		}else
			forbidden();
	});
