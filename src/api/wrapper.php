<?php
	session_start();
	include dirname(__DIR__).DIRECTORY_SEPARATOR.'autoload.php';

	function apiWrapper($closure)
	{
		try{
			$closure();
		}catch(ForbiddenException $e){
			http_response_code(403);
			exit;
		}catch(UnathorisedException $e){
			http_response_code(401);
			exit;
		}catch(Exception $e){
			echo $e->getMessage();
			http_response_code(500);
			die('api error');
		}
	}

	//auth	
	function isAdmin(){
		if(!isset($_SESSION['user'])) throw new UnathorisedException();
		return $_SESSION['user']['role'] == 'admin';
	}

	function isOwner($dataOwnerId){
		if(!isset($_SESSION['user'])) throw new UnathorisedException();
		return 	$dataOwnerId == $_SESSION['user']['id'];
	}

	function getCurrentLogin(){
		if(!isset($_SESSION['user'])) throw new UnathorisedException();
		return $_SESSION['user']['login'];		
	}

	function getCurrentUserID(){
		if(!isset($_SESSION['user'])) throw new UnathorisedException();
		return $_SESSION['user']['id'];		
	}

	//exceptions
	function forbidden(){
		throw new ForbiddenException();
	}


	//shared functions
	function getHolidays($from, $to){
		$stmt = db::getConnection()->prepare(
			"SELECT DATE_FORMAT(date, '%d.%m.%Y') 
			FROM holidays 
			WHERE 
				date BETWEEN 
					FROM_UNIXTIME(:from) 
					AND FROM_UNIXTIME(:to)");
		$stmt->bindValue(':from', $from);
		$stmt->bindValue(':to', $to);
		$stmt->execute();
	
		return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	}

	function getAllPlanableUsers(){
		$stmt = db::getConnection()->query(
			"SELECT id,username FROM users WHERE plan = 1");
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	function getPlanFor($beginDate, $endDate){
		$stmt = db::getConnection()->prepare(
			"SELECT 
				UNIX_TIMESTAMP(date) as date, 
				GROUP_CONCAT(DISTINCT username SEPARATOR ',') as usernames,
				GROUP_CONCAT(DISTINCT users.id SEPARATOR ',') as userids
			FROM plan LEFT JOIN users ON plan.involved = users.id 
			WHERE 
				date BETWEEN 
					DATE(FROM_UNIXTIME(:from)) 
					AND DATE(FROM_UNIXTIME(:to)) 
			GROUP BY date 
			ORDER BY date");
		$stmt->bindValue(':from', $beginDate);
		$stmt->bindValue(':to', $endDate);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		array_walk($result, function(&$row){
			$row['usernames'] = explode(',', $row['usernames']);
		});
		return $result;
	}

