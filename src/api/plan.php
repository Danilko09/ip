<?php
	define('DAY', 60*60*24);
	define('WEEK', 7*DAY);
	define('MONTH', 31*DAY);
	define('YEAR', 365*DAY);
	
	include 'wrapper.php';
	apiWrapper(function(){
		$from = filter_input(INPUT_GET,'from') ?? time();
		$to = filter_input(INPUT_GET, 'to') ?? time() + 3*WEEK;

		genPlanFor($from, $to);
		$users = getAllPlanableUsers();
		$plan = getPlanFor($from, $to);
		$holidays = getHolidays($from, $to);

		$table_headers = [];
		$table_data = array_flip(array_column($users, 'username'));
		array_walk($table_data, function(&$val){$val = [];});

		foreach($plan as $row){
			$table_headers[] = date('j.m', $row['date']);

			array_walk($table_data, 
				function(&$value, $username, $row) use ($holidays){
					if(in_array($username, $row['usernames'])){
						$value[] = 'i';
					}else if(isHoliday($row['date'], $holidays)){
						$value[] = 'h';
					}else{
						$value[] = '';
					}
				}, 
			$row);
		}

		$table_data[''] = $table_headers;
		echo json_encode($table_data);
	});


	function genPlanFor($from, $to){
		clearPlanForDates($from, $to);

		$result = [];

		$timestamp = $from;
		$holidays = getHolidays($from, $to);
		while(isHoliday($timestamp, $holidays)) $timestamp += DAY;

		$weekend = getWeekEnd($timestamp, $holidays);
		$involved = getInvolvedThisWeek($timestamp);
		if(count($involved) < 1) $involved = getNextInvolved(2, $from, $to, $timestamp); 

		while($to > $timestamp){
			if(isHoliday($timestamp, $holidays)){
				while(isHoliday($timestamp, $holidays)){
					planUsers($timestamp, []);
					$timestamp += DAY;

				}
				if($timestamp >= $weekend){
					$involved = getNextInvolved(2, $from, $to, $timestamp);	
					$weekend = getWeekEnd($timestamp, $holidays);
				}
			}else{	
				if(count($involved) < 1)
					planUsers($timestamp, []);
				else
					planUsers($timestamp, $involved);

				$timestamp += DAY;
			}
		}

		return $result;
	}

	function clearPlanForDates($from, $to){
		$stmt = db::getConnection()->prepare("DELETE FROM plan WHERE date BETWEEN DATE(FROM_UNIXTIME(:from)) AND DATE(FROM_UNIXTIME(:to))");
		$stmt->bindValue(':from', $from);
		$stmt->bindValue(':to', $to);
		$stmt->execute();
	}

	function planUsers($timestamp, $users){
		$stmt = db::getConnection()->prepare("INSERT INTO plan (date, involved) VALUES (DATE(FROM_UNIXTIME(:ts)), :userid)");	
		$stmt->bindValue(':ts', $timestamp);
		
		if(is_string($users['userids'])) $users['userids'] = explode(',', $users['userids']);
		if(count($users['userids']) > 0){
			foreach($users['userids'] as $userid){
				$stmt->bindValue(':userid', $userid);				
				$stmt->execute();
			}
		}else{
				$stmt->bindValue(':userid', null);				
				$stmt->execute();
		}
	}

	function getWeekEnd($timestamp){
		$holidays = getHolidays($timestamp, $timestamp + MONTH);
		if(isHoliday($timestamp, $holidays)) throw new Exception("getWeekEnd timestamp in holiday");

		$i = 0;
		while(!isHoliday($timestamp, $holidays)){
			$timestamp += DAY;
			if($i++ > 60){
				throw new Exception("getWeekEnd no holidays more than 60 days");
			}
		}
		return $timestamp;
	}

	function isHoliday($timestamp, $holidays){
		return in_array(date('j.m.Y', $timestamp), $holidays);
	}

	function getInvolvedThisWeek($timestamp){
		$holidays = getHolidays($timestamp - 2*WEEK, $timestamp+DAY);
		if(isHoliday($timestamp, $holidays)) throw new Exception("getInvolvedThisWeek timestamp in holiday");

		$involved = getPlanForDay($timestamp);

		$i = 0;
		while(!isset($involved['userids']) || count($involved['userids']) < 1){
			if($i++ == 100) throw new Exception();
			$timestamp -= DAY;
			$involved = getPlanForDay($timestamp);

			if(isHoliday($timestamp, $holidays)) return [];
		}

		return $involved;
	}
	
	function getPlanForDay($timestamp){
		$plan = getPlanFor($timestamp, $timestamp);
		return isset($plan[0]) ? $plan[0] : ['userids'=>[], 'usernames'=>[]];	
	}

	function getNextInvolved($count, $from, $to, $timestamp){
		$result = ['userids'=>[], 'usernames'=>[]];

		$prioritised = calcPriority($timestamp);
		loadNonPlanableDates($prioritised, $from, $to);

		foreach($prioritised as $user){
			if(count($result['userids']) >= $count) break;			
			$result['userids'][] = $user['id'];
			$result['usernames'][] = $user['username'];
		}

		return $result;
	}
	
	function loadNonPlanableDates(&$users, $from, $to){
		$stmt = db::getConnection()->prepare("
			SELECT DATE_FORMAT(date, '%d.%m.%Y') 
			FROM no_planning 
			WHERE userid = :uid 
				AND date BETWEEN 
					DATE(FROM_UNIXTIME(:from)) and 
					DATE(FROM_UNIXTIME(:to))
		");
		$stmt->bindValue(':from', $from);
		$stmt->bindValue(':to', $to);

		foreach($users as &$user){
			$stmt->bindValue(':uid', $user['id']);
			$stmt->execute();
			$user['no_planning'] = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);		
		}	
	}

	function calcPriority($ts){
		$stmt = db::getConnection()->prepare("SELECT id, username, (COUNT(date) + (:dutyPeriod - DATEDIFF(DATE(FROM_UNIXTIME(:ts)), MAX(date)))*:k) as priority
			FROM users LEFT JOIN plan ON id = involved 
			WHERE users.plan = 1 AND (date BETWEEN DATE(FROM_UNIXTIME(:from)) AND DATE(FROM_UNIXTIME(:ts)) OR date IS NULL)
			GROUP BY involved
			ORDER BY priority");	
		$stmt->bindValue(':ts', $ts);
		$stmt->bindValue(':from', time() - YEAR);	
		$stmt->bindValue(':dutyPeriod', 30);	
		$stmt->bindValue(':k', 7);	
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
