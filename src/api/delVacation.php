<?php
	include 'wrapper.php';
	apiWrapper(function(){
		$userid = empty($_POST['userid']) ? getCurrentUserID() : $_POST['userid'];//throws HTTP 401
		$date = filter_input(INPUT_POST,'date') ?? time();

		if(isAdmin() || empty($_POST['userid'])){
			$stmt = db::getConnection()->prepare("DELETE FROM no_planning WHERE userid = :uid AND date = STR_TO_DATE(:date,'%e.%c.%Y') AND type = 'vacation'");
			$stmt->bindValue(':uid', $userid);
			$stmt->bindValue(':date', $date);
			$stmt->execute();
		}else
			forbidden();
	});
