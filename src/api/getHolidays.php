<?php
	include 'wrapper.php';
	apiWrapper(function(){
		$from = filter_input(INPUT_GET,'from') ?? time();
		$to = filter_input(INPUT_GET, 'to') ?? time(); 

		echo json_encode(getHolidays($from, $to));
	});
