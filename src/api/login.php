<?php
	include 'wrapper.php';

	apiWrapper(function(){
		if(isset($_SESSION['user'])){return;}

		$login = filter_input(INPUT_POST, 'login');
		$passw = filter_input(INPUT_POST, 'password');
		
		$stmt = db::getConnection()->prepare('SELECT * FROM users WHERE login = :login');
		$stmt->bindValue(':login', $login);
		$stmt->execute();

		if($stmt->rowCount() == 1){
			$user = $stmt->fetch(PDO::FETCH_ASSOC);
			if(password_verify($passw, $user['password'])){
				$_SESSION['user'] = $user;
				echo 'success';
				return;
			}
		}
		echo 'error';
	});
