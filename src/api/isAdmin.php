<?php
	include 'wrapper.php';
	apiWrapper(function(){
		try {
			echo json_encode(isAdmin());
		}catch(UnathorisedException $e){
			echo json_encode(false);
		}
	});
