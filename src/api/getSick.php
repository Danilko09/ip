<?php
	include 'wrapper.php';
	apiWrapper(function(){
		$uid = empty($_GET['userid']) ? getCurrentUserID() : $_GET['userid'];//throws HTTP 401

		$from = filter_input(INPUT_GET,'from') ?? time();
		$to = filter_input(INPUT_GET, 'to') ?? time(); 

		if(isAdmin() || empty($_GET['userid'])){
			$stmt = db::getConnection()->prepare("SELECT DATE_FORMAT(date, '%e.%c.%Y') FROM no_planning WHERE userid = :uid AND type = 'sick' AND date BETWEEN FROM_UNIXTIME(:from) and FROM_UNIXTIME(:to)");
			$stmt->bindValue(':uid', $uid);
			$stmt->bindValue(':from', $from);
			$stmt->bindValue(':to', $to);
			$stmt->execute();

			echo json_encode($stmt->fetchAll(PDO::FETCH_COLUMN, 0));
		}else
			forbidden();
	});
