<?php
	spl_autoload_register(function ($name) {
		$filename = __DIR__.DIRECTORY_SEPARATOR.'classes/'.$name.'.php';
		if(file_exists($filename))
			include $filename;
	});
