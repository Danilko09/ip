-- --------------------------------------------------------
-- Хост:                         192.168.99.100
-- Версия сервера:               5.5.62 - MySQL Community Server (GPL)
-- Операционная система:         linux-glibc2.12
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных schedule
CREATE DATABASE IF NOT EXISTS `schedule` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `schedule`;

-- Дамп структуры для таблица schedule.history
CREATE TABLE IF NOT EXISTS `history` (
  `date` date NOT NULL,
  `involved` varchar(50) NOT NULL,
  KEY `Индекс 1` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица schedule.holidays
CREATE TABLE IF NOT EXISTS `holidays` (
  `date` date NOT NULL,
  UNIQUE KEY `Индекс 1` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица schedule.no_planning
CREATE TABLE IF NOT EXISTS `no_planning` (
  `userid` int(11) NOT NULL,
  `date` date NOT NULL,
  `type` enum('vacation','sick') NOT NULL,
  KEY `FK__users` (`userid`),
  CONSTRAINT `FK__users` FOREIGN KEY (`userid`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица schedule.plan
CREATE TABLE IF NOT EXISTS `plan` (
  `date` date NOT NULL,
  `involved` int(11) DEFAULT NULL,
  UNIQUE KEY `Индекс 3` (`involved`,`date`),
  KEY `Индекс 1` (`date`),
  CONSTRAINT `FK_plan_users` FOREIGN KEY (`involved`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица schedule.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `role` varchar(50) NOT NULL,
  `plan` bit(1) NOT NULL,
  UNIQUE KEY `Индекс 2` (`login`),
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
